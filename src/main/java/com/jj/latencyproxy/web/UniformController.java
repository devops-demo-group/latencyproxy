package com.jj.latencyproxy.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.jj.latencyproxy.domain.ResponseTime;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UniformController {
	
	@Value("${latancy.server}")
	private String latencyServer;
	
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/uniform")
	@ResponseBody
	@Timed(histogram = true)
	public ResponseTime uniform(@RequestParam(name = "latency", required = true) long latency)
			throws InterruptedException {
		log.trace("UniformController.uniform");
		
		final Map<String, Long> params = new HashMap<String, Long>();
	    params.put("latency", latency);
		
		final ResponseTime responseTime = restTemplate.getForObject(
				latencyServer + "/uniform?latency={latency}", ResponseTime.class, params);
		log.info(responseTime.toString());
		
		return responseTime;
	}
}
package com.jj.latencyproxy;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class LatencyproxyApplication {

	@Value("${http-pool.maxTotal}")
	private String httpPoolMaxTotal;

	@Value("${http-pool.defaultMaxPerRoute}")
	private String httpPoolMaxPerRoute;

	@Value("${http-pool.connectionRequestTimeout}")
	private String httpPoolConnectionRequestTimeout;

	@Value("${http-pool.connectTimeout}")
	private String httpPoolConnectTimeout;

	@Value("${http-pool.socketTimeout}")
	private String httpPoolSocketTimeout;
	
	@Value("${http-pool.validateAfterInactivity}")
	private String httpPoolValidateAfterInactivity;

	@Bean
	public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
		PoolingHttpClientConnectionManager result = new PoolingHttpClientConnectionManager();
		result.setMaxTotal(Integer.parseInt(httpPoolMaxTotal));
		result.setDefaultMaxPerRoute(Integer.parseInt(httpPoolMaxPerRoute));
		result.setValidateAfterInactivity(Integer.parseInt(httpPoolValidateAfterInactivity));
		return result;
	}

	@Bean
	public RequestConfig requestConfig() {
		RequestConfig result = RequestConfig.custom()
				.setConnectionRequestTimeout(Integer.parseInt(httpPoolConnectionRequestTimeout))
				.setConnectTimeout(Integer.parseInt(httpPoolConnectTimeout))
				.setSocketTimeout(Integer.parseInt(httpPoolSocketTimeout)).build();
		return result;
	}

	@Bean
	public CloseableHttpClient httpClient(PoolingHttpClientConnectionManager poolingHttpClientConnectionManager,
			RequestConfig requestConfig) {
		CloseableHttpClient result = HttpClientBuilder.create().setConnectionManager(poolingHttpClientConnectionManager)
				.setDefaultRequestConfig(requestConfig).build();
		return result;
	}

	@Bean
	public RestTemplate restTemplate(HttpClient httpClient) {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
		// requestFactory.setHttpClient(httpClient);
		return new RestTemplate(requestFactory);
	}

	/*
	 * @Bean public RestTemplate restTemplate(RestTemplateBuilder builder) { return
	 * builder.build(); }
	 */
	public static void main(String[] args) {
		SpringApplication.run(LatencyproxyApplication.class, args);
	}

}

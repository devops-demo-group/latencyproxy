package com.jj.latencyproxy;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.MDC;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@Configuration
public class RequestLoggingFilterConfig {
	
	private static class DurationLoggerFilter extends CommonsRequestLoggingFilter {
		private final static String name = "duration";
		
		/**
		 * Writes a log message before the request is processed.
		 */
		@Override
		protected void beforeRequest(HttpServletRequest request, String message) {
			super.beforeRequest(request, message);
			
			request.setAttribute(DurationLoggerFilter.name, java.lang.System.currentTimeMillis());
		}

		/**
		 * Writes a log message after the request is processed.
		 */
		@Override
		protected void afterRequest(HttpServletRequest request, String message) {
			try {
				final long duration = java.lang.System.currentTimeMillis() - (Long) request.getAttribute(DurationLoggerFilter.name);
				MDC.put(DurationLoggerFilter.name, String.valueOf(duration));
				
				super.afterRequest(request, message);
			} finally {
				MDC.remove(DurationLoggerFilter.name);
			}
		}
	}
 
    @Bean
    public CommonsRequestLoggingFilter logFilter() {
        CommonsRequestLoggingFilter filter
          = new DurationLoggerFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(true);
        filter.setAfterMessagePrefix("REQUEST DATA : ");
        return filter;
    }
}